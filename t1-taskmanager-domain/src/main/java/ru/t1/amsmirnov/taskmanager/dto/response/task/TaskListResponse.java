package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public final class TaskListResponse extends AbstractTaskListResponse {

    public TaskListResponse() {
    }


    public TaskListResponse(@Nullable final List<Task> tasks) {
        super(tasks);
    }

    public TaskListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}