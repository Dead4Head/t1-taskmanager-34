package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

public final class TaskStartByIndexResponse extends AbstractTaskResponse {

    public TaskStartByIndexResponse() {
    }

    public TaskStartByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskStartByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
