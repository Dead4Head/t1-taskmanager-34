package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
