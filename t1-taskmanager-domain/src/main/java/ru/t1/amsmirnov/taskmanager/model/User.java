package ru.t1.amsmirnov.taskmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

public final class User extends AbstractModel {

    @NotNull
    @Getter
    @Setter
    private String login;

    @NotNull
    @Getter
    @Setter
    private String passwordHash;

    @Nullable
    @Getter
    @Setter
    private String email;

    @Nullable
    @Getter
    @Setter
    private String firstName;

    @Nullable
    @Getter
    @Setter
    private String lastName;

    @Nullable
    @Getter
    @Setter
    private String middleName;

    @NotNull
    @Getter
    @Setter
    private Role role = Role.USUAL;

    @Getter
    @Setter
    private boolean locked = false;

}
