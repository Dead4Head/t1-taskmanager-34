package ru.t1.amsmirnov.taskmanager.exception;

import org.jetbrains.annotations.NotNull;

public final class CommandException extends AbstractException {

    public CommandException(@NotNull final String message) {
        super(message);
    }

}
