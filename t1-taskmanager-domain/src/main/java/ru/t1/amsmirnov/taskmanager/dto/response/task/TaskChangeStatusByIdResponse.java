package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse() {
    }

    public TaskChangeStatusByIdResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskChangeStatusByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}