package ru.t1.amsmirnov.taskmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.model.IWBS;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Getter
    @Setter
    private String name = "";

    @Nullable
    @Getter
    @Setter
    private String description = "";

    @NotNull
    @Getter
    @Setter
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Getter
    @Setter
    private String projectId;

    @NotNull
    private Date created = new Date();

    @Override
    @NotNull
    public String toString() {
        return name + " : " + description;
    }

    @Override
    @NotNull
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(@NotNull final Date created) {
        this.created = created;
    }

}
