package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

public final class UserRemoveByEmailResponse extends AbstractUserResponse {

    public UserRemoveByEmailResponse() {
    }

    public UserRemoveByEmailResponse(@Nullable final User user) {
        super(user);
    }

    public UserRemoveByEmailResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}