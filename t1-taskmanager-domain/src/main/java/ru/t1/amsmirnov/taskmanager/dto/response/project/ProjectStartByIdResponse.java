package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Project;

public final class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse() {
    }

    public ProjectStartByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectStartByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
