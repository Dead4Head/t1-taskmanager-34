package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface IDomainService {

    @NotNull
    String FILE_BACKUP = "./backup.base64";

    void loadDataBackup() throws AbstractException;

    void saveDataBackup() throws AbstractException;

    void loadDataBase64() throws AbstractException;

    void saveDataBase64() throws AbstractException;

    void loadDataBin() throws AbstractException;

    void saveDataBin() throws AbstractException;

    void loadDataJsonFasterXML() throws AbstractException;

    void saveDataJsonFasterXML() throws AbstractException;

    void loadDataJsonJaxB() throws AbstractException;

    void saveDataJsonJaxB() throws AbstractException;

    void loadDataXMLFasterXML() throws AbstractException;

    void saveDataXMLFasterXML() throws AbstractException;

    void loadDataXMLJaxB() throws AbstractException;

    void saveDataXMLJaxB() throws AbstractException;

    void loadDataYAMLFasterXML() throws AbstractException;

    void saveDataYAMLFasterXML() throws AbstractException;

}
