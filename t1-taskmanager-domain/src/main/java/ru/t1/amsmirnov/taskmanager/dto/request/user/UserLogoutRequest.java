package ru.t1.amsmirnov.taskmanager.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest() {
    }

    public UserLogoutRequest(@Nullable final String token) {
        super(token);
    }

}
