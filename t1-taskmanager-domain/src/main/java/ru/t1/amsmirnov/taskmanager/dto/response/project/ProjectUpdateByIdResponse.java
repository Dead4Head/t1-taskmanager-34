package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Project;

public final class ProjectUpdateByIdResponse extends AbstractProjectResponse {

    public ProjectUpdateByIdResponse() {
    }

    public ProjectUpdateByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}