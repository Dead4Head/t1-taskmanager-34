package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public final class TaskListByProjectIdResponse extends AbstractTaskListResponse {

    public TaskListByProjectIdResponse() {
    }

    public TaskListByProjectIdResponse(@Nullable final List<Task> tasks) {
        setTasks(tasks);
    }

    public TaskListByProjectIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}