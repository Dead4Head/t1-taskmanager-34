package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

public final class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse() {
    }

    public TaskShowByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskShowByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}