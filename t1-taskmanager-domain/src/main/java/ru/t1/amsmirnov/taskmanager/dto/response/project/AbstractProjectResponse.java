package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.model.Project;

public abstract class AbstractProjectResponse extends AbstractResultResponse {

    @Nullable
    private Project project;

    protected AbstractProjectResponse() {
    }

    protected AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

    protected AbstractProjectResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public Project getProject() {
        return project;
    }

    public void setProject(@Nullable final Project project) {
        this.project = project;
    }

}
